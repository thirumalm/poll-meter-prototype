import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule,ReactiveFormsModule } from '@angular/forms';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';


import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { MyOwnCustomMaterialModule } from './custom.material.module';
import { ChartsModule } from 'ng2-charts';
import { LoginComponent } from './login/login/login.component';
import { ForgotPasswordComponent } from './login/forgot-password/forgot-password.component';
import { TopHeaderComponent } from './header/top-header/top-header.component';
import { SubHeaderComponent } from './header/sub-header/sub-header.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ContentComponent } from './content/content.component';
import { BarChartComponent } from './content/chart/bar-chart/bar-chart.component';
import { FooterComponent } from './footer/footer.component';
import { PieChartComponent } from './content/chart/pie-chart/pie-chart.component';
import { DoughnutChartComponent } from './content/chart/doughnut-chart/doughnut-chart.component';
import { LineChartComponent } from './content/chart/line-chart/line-chart.component';
import { RadarChartComponent } from './content/chart/radar-chart/radar-chart.component';
import { VoterDispositionComponent } from './content/voter-disposition/voter-disposition.component';
import { VoterProfileComponent } from './content/voter-profile/voter-profile.component';
import { WorkersActivityComponent } from './content/workers-activity/workers-activity.component';
import { TopIssuesComponent } from './content/top-issues/top-issues.component';
import { SocialMediaMgtComponent } from './content/social-media-mgt/social-media-mgt.component';
import { VotingDayComponent } from './content/voting-day/voting-day.component';
import { VoterDataComponent } from './datasource/voter-data/voter-data.component';
import { ClientDataComponent } from './datasource/client-data/client-data.component';
import { SignUpComponent } from './login/sign-up/sign-up.component';
import { ChangePasswordComponent } from './login/change-password/change-password.component';
import { ClientUserComponent } from './user-creation/client-user/client-user.component';
import {RatingModule} from "ngx-rating";
import { AccountSettingsComponent } from './user-creation/account-settings/account-settings.component';
import { MapComponent } from './content/map/map.component';
import { SearchConditionPipe } from './common/search-condition.pipe';
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    ForgotPasswordComponent,
    TopHeaderComponent,
    SubHeaderComponent,
    DashboardComponent,
    ContentComponent,
    BarChartComponent,
    FooterComponent,
    PieChartComponent,
    DoughnutChartComponent,
    LineChartComponent,
    RadarChartComponent,
    VoterDispositionComponent,
    VoterProfileComponent,
    WorkersActivityComponent,
    TopIssuesComponent,
    SocialMediaMgtComponent,
    VotingDayComponent,
    VoterDataComponent,
    ClientDataComponent,
    SignUpComponent,
    ChangePasswordComponent,
    ClientUserComponent,
    AccountSettingsComponent,
    MapComponent,
    SearchConditionPipe
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    MyOwnCustomMaterialModule,
    ChartsModule,
    FormsModule,
    ReactiveFormsModule,
    RatingModule
  ],
  providers: [],
  bootstrap: [AppComponent]
})
export class AppModule { }

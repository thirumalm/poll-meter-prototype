import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  userName: string;
  password: string;
  isUserValid=true;
  constructor(
    private myRoute: Router,
  ) { }

  ngOnInit() {
  }
  public loginValidation(event) {
    // console.log(this.userName, this.password);

    if (this.password === 'admin' && this.userName === 'admin') {
      this.myRoute.navigate(['home','dashboard']);
    } else {
      this.isUserValid=false;
      this.myRoute.navigate(['reset-passward']);
      // this.myRoute.navigate(['404']);
    }
  }

}

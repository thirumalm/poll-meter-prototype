import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoterDispositionComponent } from './voter-disposition.component';

describe('VoterDispositionComponent', () => {
  let component: VoterDispositionComponent;
  let fixture: ComponentFixture<VoterDispositionComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoterDispositionComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoterDispositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

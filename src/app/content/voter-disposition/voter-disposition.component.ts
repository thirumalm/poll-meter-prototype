import { Component, OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

@Component({
  selector: 'app-voter-disposition',
  templateUrl: './voter-disposition.component.html',
  styleUrls: ['./voter-disposition.component.scss']
})
export class VoterDispositionComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }
  pieChartLabels = ['Fully against', 'Partially against', 'Undecided', 'Partially loyal','Fully loyal'];
  pieChartData= [20, 10, 25, 25,30];
  backgroundColor=[{backgroundColor: ["#e84351", "#434a54", "#3ebf9b", "#4d86dc", "#f3af37","43bfdc"]}];

  pieChartLabels1 = ['Voted', 'Not Voted'];
  pieChartData1= [70, 30];
  backgroundColo1r=[{backgroundColor: ["green", "red"]}];

  pieChartLabels2 = ['Yes', 'No'];
  pieChartData2= [65, 35];
  backgroundColor2=[{backgroundColor: ["blue", "green"]}];



   barChartLabelsLastElection = ["2019"];
   barChartType = "horizontalBar";
   barChartDataLastElection = [
    { data: [27], label: "BJP", backgroundColor: "orange" },
    { data: [2], label: "INC", backgroundColor: "blue" },
    { data: [15], label: "BSP", backgroundColor: "red" },
    { data: [-8], label: "SP", backgroundColor: "pink" },
    { data: [2], label: "TMC", backgroundColor: "yellow" },
    { data: [1], label: "IND", backgroundColor: "blue" },
    { data: [8], label: "Other", backgroundColor: "blue" }
  ];
  barChartLabelsThisElection = ["2024"];
   barChartDataThisElection = [
    { data: [2], label: "BJP", backgroundColor: "orange" },
    { data: [20], label: "INC", backgroundColor: "blue" },
    { data: [-7], label: "BSP", backgroundColor: "red" },
    { data: [-8], label: "SP", backgroundColor: "pink" },
    { data: [2], label: "TMC", backgroundColor: "yellow" },
    { data: [15], label: "IND", backgroundColor: "blue" },
    { data: [8], label: "Other", backgroundColor: "blue" }
  ];
  barChartLabelsIssue = ["2019"];
  barChartDataThisIssue = [
   { data: [42], label: "Issues 1", backgroundColor: "orange" },
   { data: [20], label: "Issues 2", backgroundColor: "blue" },
   { data: [17], label: "Issues 3", backgroundColor: "red" },
   { data: [28], label: "Issues 4", backgroundColor: "pink" },
   { data: [10], label: "Issues 5", backgroundColor: "yellow" },
 ];

 barChartLabelsInfluenced = ["2019"];
 barChartDataInfluenced = [
  { data: [42], label: "Spouse", backgroundColor: "orange" },
  { data: [20], label: "Parents", backgroundColor: "yellow" },
  { data: [17], label: "Friends", backgroundColor: "red" },
  { data: [28], label: "Social", backgroundColor: "pink" },
  { data: [10], label: "Other", backgroundColor: "red" },
];

  toppings = new FormControl();
  toppings1 = new FormControl();
  userType:any;
  constituencies=[
    {id: 1, name: "Narela"},
    {id: 2, name: "Burari"},
    {id: 3, name: "Timarpur"},
    {id: 4, name: "Adarsh Nagar"},
    {id: 5, name: "Badli"},
    {id: 6, name: "Rithala"},
    {id: 7, name: "Bawana"},
    {id: 8, name: "Mundka"},
    {id: 9, name: "Kirari"},
    {id: 10, name: "Sultan Pur Majra"},
    {id: 11, name: "Nangloi Jat"},
    {id: 12, name: "Mangol Puri"},
    {id: 13, name: "Rohini"},
    {id: 14, name: "Shalimar Bagh"},
    {id: 15, name: "Shakur Basti"},
    {id: 16, name: "Tri Nagar"},
    {id: 17, name: "Wazirpur"},
    {id: 18, name: "Model Town"},
    {id: 19, name: "Sadar Bazar"},
    {id: 20, name: "Chandni Chowk"},
    {id: 21, name: "Matia Mahal"},
    {id: 22, name: "Ballimaran"},
    {id: 23, name: "Karol Bagh"},
    {id: 24, name: "Patel Nagar"},
    {id: 25, name: "Moti Nagar"},
    {id: 26, name: "Madipur"},
    {id: 27, name: "Rajouri Garden"},
    {id: 28, name: "Hari Nagar"},
    {id: 29, name: "Tilak Nagar"},
    {id: 30, name: "Janakpuri"},
    {id: 31, name: "Vikaspuri"},
    {id: 32, name: "Uttam Nagar"},
    {id: 33, name: "Dwarka"},
    {id: 34, name: "Matiala"},
    {id: 35, name: "Najafgarh"},
    {id: 36, name: "Bijwasan"},
    {id: 37, name: "Palam"},
    {id: 38, name: "Delhi Cantonment"},
    {id: 39, name: "Rajinder Nagar"},
    {id: 40, name: "New Delhi"},
    {id: 41, name: "Jangpura"},
    {id: 42, name: "Kasturba Nagar"},
    {id: 43, name: "Malviya Nagar"},
    {id: 44, name: "R K Puram"},
    {id: 45, name: "Mehrauli"},
    {id: 46, name: "Chhatarpur"},
    {id: 47, name: "Deoli"},
    {id: 48, name: "Ambedkar Nagar"},
    {id: 49, name: "Sangam Vihar"},
    {id: 50, name: "Greater Kailash"},
    {id: 51, name: "Kalkaji"},
    {id: 52, name: "Tughlkabad extension"},
    {id: 53, name: "Badarpur"},
    {id: 54, name: "Okhla"},
    {id: 55, name: "Trilokpuri"},
    {id: 56, name: "Kondli"},
    {id: 57, name: "Patparganj"},
    {id: 58, name: "Laxmi Nagar"},
    {id: 59, name: "Vishwas Nagar"},
    {id: 60, name: "Krishna Nagar"},
    {id: 61, name: "Gandhi Nagar"},
    {id: 62, name: "Shahdara"},
    {id: 63, name: "Seemapuri"},
    {id: 64, name: "Rohtas Nagar"},
    {id: 65, name: "Seelampur"},
    {id: 66, name: "Ghonda"},
    {id: 67, name: "Babarpur"},
    {id: 68, name: "Gokalpur"},
    {id: 69, name: "Mustafabad"},
    {id: 70, name: "Karawal Nagar"},

  ];
  stateList = [
    {
      key: "AN",
      name: "Andaman and Nicobar Islands"
    },
    {
      key: "AP",
      name: "Andhra Pradesh"
    },
    {
      key: "AR",
      name: "Arunachal Pradesh"
    },
    {
      key: "AS",
      name: "Assam"
    },
    {
      key: "BR",
      name: "Bihar"
    },
    {
      key: "CG",
      name: "Chandigarh"
    },
    {
      key: "CH",
      name: "Chhattisgarh"
    },
    {
      key: "DH",
      name: "Dadra and Nagar Haveli"
    },
    {
      key: "DD",
      name: "Daman and Diu"
    },
    {
      key: "DL",
      name: "Delhi"
    },
    {
      key: "GA",
      name: "Goa"
    },
    {
      key: "GJ",
      name: "Gujarat"
    },
    {
      key: "HR",
      name: "Haryana"
    },
    {
      key: "HP",
      name: "Himachal Pradesh"
    },
    {
      key: "JK",
      name: "Jammu and Kashmir"
    },
    {
      key: "JH",
      name: "Jharkhand"
    },
    {
      key: "KA",
      name: "Karnataka"
    },
    {
      key: "KL",
      name: "Kerala"
    },
    {
      key: "LD",
      name: "Lakshadweep"
    },
    {
      key: "MP",
      name: "Madhya Pradesh"
    },
    {
      key: "MH",
      name: "Maharashtra"
    },
    {
      key: "MN",
      name: "Manipur"
    },
    {
      key: "ML",
      name: "Meghalaya"
    },
    {
      key: "MZ",
      name: "Mizoram"
    },
    {
      key: "NL",
      name: "Nagaland"
    },
    {
      key: "OR",
      name: "Odisha"
    },
    {
      key: "PY",
      name: "Puducherry"
    },
    {
      key: "PB",
      name: "Punjab"
    },
    {
      key: "RJ",
      name: "Rajasthan"
    },
    {
      key: "SK",
      name: "Sikkim"
    },
    {
      key: "TN",
      name: "Tamil Nadu"
    },
    {
      key: "TS",
      name: "Telangana"
    },
    {
      key: "TR",
      name: "Tripura"
    },
    {
      key: "UK",
      name: "Uttar Pradesh"
    },
    {
      key: "UP",
      name: "Uttarakhand"
    },
    {
      key: "WB",
      name: "West Bengal"
    }
  ];
  tappinagValue(){
    console.log("tappinagValue ",this.toppings.value);

  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SocialMediaMgtComponent } from './social-media-mgt.component';

describe('SocialMediaMgtComponent', () => {
  let component: SocialMediaMgtComponent;
  let fixture: ComponentFixture<SocialMediaMgtComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SocialMediaMgtComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SocialMediaMgtComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

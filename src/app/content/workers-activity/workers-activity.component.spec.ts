import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WorkersActivityComponent } from './workers-activity.component';

describe('WorkersActivityComponent', () => {
  let component: WorkersActivityComponent;
  let fixture: ComponentFixture<WorkersActivityComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WorkersActivityComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WorkersActivityComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

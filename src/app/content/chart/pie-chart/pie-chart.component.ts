import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'app-pie-chart',
  templateUrl: './pie-chart.component.html',
  styleUrls: ['./pie-chart.component.scss']
})
export class PieChartComponent implements OnInit {
  @Input() pieChartLabels
  @Input() pieChartData;
  public pieChartType = 'pie';
  @Input() backgroundColor;
  constructor() { }

  ngOnInit() {
  }

}

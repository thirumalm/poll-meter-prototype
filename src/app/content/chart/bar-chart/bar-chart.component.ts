import { Component, OnInit, Input } from "@angular/core";

@Component({
  selector: "app-bar-chart",
  templateUrl: "./bar-chart.component.html",
  styleUrls: ["./bar-chart.component.scss"]
})
export class BarChartComponent implements OnInit {
  constructor() {}
  public barChartOptions = {
    scaleShowVerticalLines: true,
    responsive: true
    // "scales": {
    //   "xAxes": [{
    //     "ticks": {
    //       "beginAtZero": true
    //     }
    //   }]
    // }
  };
  @Input() barChartLabels;
  @Input() barChartType ;
  public barChartLegend = true;
  // public backgroundColor=[{backgroundColor: ["red", "blue"]}]
  @Input() barChartData;
  ngOnInit() {}
}

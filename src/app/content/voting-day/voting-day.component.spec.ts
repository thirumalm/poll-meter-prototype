import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VotingDayComponent } from './voting-day.component';

describe('VotingDayComponent', () => {
  let component: VotingDayComponent;
  let fixture: ComponentFixture<VotingDayComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VotingDayComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VotingDayComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

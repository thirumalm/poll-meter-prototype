import { Component, OnInit } from "@angular/core";
import {
  FormGroup,
  FormBuilder,
  Validators,
  FormControl
} from "@angular/forms";

export interface Food {
  value: string;
  viewValue: string;
}

@Component({
  selector: "app-voting-day",
  templateUrl: "./voting-day.component.html",
  styleUrls: ["./voting-day.component.scss"]
})
export class VotingDayComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  isEditable = true;
  SearchKeylokSabha
  SearchKeyLokSabhaNo
  SearchKeyVidhanSabha
  SearchKeyVidhanSabhaNo
  SearchKeyPollingBooth
  SearchKeyVillageName
  SearchKeyWardNo
  SearchKeyLocality
  SearchKeyName
  SearchKeyAddress

  LokSabhaNo = [
    { id: 1, value: "1" },
    { id: 2, value: "2" },
    { id: 3, value: "3" },
    { id: 4, value: "4" },
    { id: 5, value: "5" },
    { id: 5, value: "6" },
    { id: 5, value: "7" },
    { id: 5, value: "8" },
    { id: 5, value: "9" },
    { id: 5, value: "10" },
    { id: 5, value: "11" },
    { id: 5, value: "12" },
    { id: 5, value: "13" },

  ];
  Village=[
    { id: 1, value: "Shakar Pur Baramad	" },
    { id: 2, value: "Chilla Saroda Bangar" },
    { id: 3, value: "Chilla Saroda Khadar" },
    { id: 4, value: "Kair" },
    { id: 5, value: "Kangan Heri" },
    { id: 6, value: "Kharkhari Jatmal" },
    { id: 7, value: "Kharkhari Nahar" },
    { id: 8, value: "Kharkhari Rond" },
    { id: 9, value: "Khera" },
    { id: 10, value: "Khera Dabar" },
    { id: 11, value: "Malik Pur zer-Najafgarh" },
    { id: 12, value: "Mitraon" },
    { id: 13, value: "Mundhela Kalan" },
    { id: 14, value: "Mundhela Khurd" },
    { id: 15, value: "Nanak Heri" },

  ];
  Address=[
    { id: 1, value: "Shakar Pur Baramad	" },
    { id: 2, value: "Chilla Saroda Bangar" },
    { id: 3, value: "Chilla Saroda Khadar" },
    { id: 4, value: "Kair" },
    { id: 5, value: "Kangan Heri" },
    { id: 6, value: "Kharkhari Jatmal" },
    { id: 7, value: "Kharkhari Nahar" },
    { id: 8, value: "Kharkhari Rond" },
    { id: 9, value: "Khera" },
    { id: 10, value: "Khera Dabar" },
    { id: 11, value: "Malik Pur zer-Najafgarh" },
    { id: 12, value: "Mitraon" },
    { id: 13, value: "Mundhela Kalan" },
    { id: 14, value: "Mundhela Khurd" },
    { id: 15, value: "Nanak Heri" },

  ];
  voterName=[
    { id: 1, value: "SENTA ARVIND DEVJIBHAI" },
    { id: 2, value: "ASHANK KUMAR SINGH" },
    { id: 3, value: "DHRUV DHAIVAT KETANBHAI" },
    { id: 4, value: "YADAV PRAMODKUMAR RAMAKANT" },
    { id: 5, value: "DHILA SHANTIBEN VELA" },
    { id: 6, value: "VISMAY A BHARAI" },
    { id: 7, value: "RAJAT KUMAR DAMACHYA" },
    { id: 8, value: "DESAI ANKUR BALDEVBHAI" },
    { id: 9, value: "AMIT" },
    { id: 10, value: "PATEL PRASHANT ARVINDBHAI" },
    { id: 11, value: "MADAN LAL DELU " },
    { id: 12, value: "SANJAY MEENA" },
    { id: 13, value: "JHAVERI VISHAL JAYANTBHAI" },
    { id: 14, value: "Mundhela Khurd" },
    { id: 15, value: "SANJAY" },

  ];
  foods: Food[] = [
    { value: "steak-0", viewValue: "Steak1" },
    { value: "pizza-1", viewValue: "Pizza" },
    { value: "tacos-2", viewValue: "Tacos" },
    { value: "steak-0", viewValue: "Steak2" },
    { value: "pizza-1", viewValue: "Pizza" },
    { value: "tacos-2", viewValue: "Tacos" },
    { value: "steak-0", viewValue: "Steak" },
    { value: "pizza-1", viewValue: "Pizza" },
    { value: "tacos-2", viewValue: "Tacos" },
    { value: "tacos-2", viewValue: "tes" }
  ];
  favoriteSeason: string = "Male";
  seasons: string[] = ["Winter", "Spring", "Summer", "Autumn"];
  InfluenceToVote = [
    { id: 1, value: "Spouse" },
    { id: 2, value: "Parents" },
    { id: 3, value: "Other family member" },
    { id: 4, value: "Friends / relative" },
    { id: 5, value: "Electronic media" },
    { id: 6, value: "Print Media" },
    { id: 7, value: "Social media" },
    { id: 8, value: "Social worker of the society" },
    { id: 9, value: "Politically active person in the society" }
  ];
  nationalIssues = [
    { id: 1, value: "Issues 1" },
    { id: 2, value: "Issues 2" },
    { id: 3, value: "Issues 3" },
    { id: 4, value: "Issues 4" },
    { id: 5, value: "Issues 5" }
  ];
  impactCounu = [
    {
      questions: "Candidate is member of my community/ caste/ religion",
      impacts: 5
    },
    { questions: "Candidate is known from many years", impacts: 2 },
    { questions: "Candidate is local- from my area", impacts: 7 },
    { questions: "I have personal connect with the candidate", impacts: 1 },
    {
      questions: "Candidate is ready to help us to solve all our problems",
      impacts: ""
    },
    { questions: "Candidate is very Popular", impacts: 2 },
    { questions: "Candidate with strong ideology", impacts: 8 },
    { questions: "Candidate is Non- corrupt/ honest image", impacts: 1 },
    { questions: "I will Analyse the candidates past performance", impacts: 4 },
    { questions: "Based on suggestion from family member", impacts: 3 },
    { questions: "Based on suggestion from community people", impacts: 9 },
    { questions: "Candidate must be active in the area", impacts: 10 },
    { questions: "Candidate is Always available in the area", impacts: 4 }
  ];
  toppings = new FormControl();
  toppings1 = new FormControl();
  constructor(private _formBuilder: FormBuilder) {}

  politicalParties = [
    { id: 1, value: "Climate change / destruction of nature" },
    { id: 2, value: "Large scale conflict / wars" },
    { id: 3, value: "Inequality (income, discrimination) (30.8%)" },
    { id: 4, value: "Poverty" },
    {
      id: 5,
      value: "Government accountability and transparency / corruption "
    },
    { id: 5, value: "Religious conflicts" }
  ];

  occupationList = [
    { id: 1, value: "Unemployed" },
    { id: 2, value: "Student" },
    { id: 3, value: "Housewife" },
    { id: 4, value: "Retired" },
    { id: 5, value: "Unskilled worker" },
    { id: 6, value: "Skilled worker" },
    { id: 7, value: "Clerical- Private" },
    { id: 8, value: "Clerical- state Govt" },
    { id: 9, value: "Clerical- Central Govt" },
    { id: 10, value: "Supervisor- Pvt" },
    { id: 11, value: "Business/ Shop" },
    { id: 12, value: "Officer" },
    { id: 13, value: "Famer" },
    { id: 14, value: "Domestic worker" },
    { id: 15, value: "Agriculture worker" }
  ];
  qualificationList = [
    { id: 1, value: "Illiterate" },
    { id: 2, value: "Upto 7" },
    { id: 3, value: "Upto 10" },
    { id: 4, value: "12th" },
    { id: 5, value: "Some college" },
    { id: 6, value: "Graduate/ Post Grad" },
    { id: 7, value: "Prof. – Doc. / Eng" },
    { id: 8, value: "Any other Prof" }
  ];
  stateList = [
    {
      key: "AN",
      name: "Andaman and Nicobar Islands"
    },
    {
      key: "AP",
      name: "Andhra Pradesh"
    },
    {
      key: "AR",
      name: "Arunachal Pradesh"
    },
    {
      key: "AS",
      name: "Assam"
    },
    {
      key: "BR",
      name: "Bihar"
    },
    {
      key: "CG",
      name: "Chandigarh"
    },
    {
      key: "CH",
      name: "Chhattisgarh"
    },
    {
      key: "DH",
      name: "Dadra and Nagar Haveli"
    },
    {
      key: "DD",
      name: "Daman and Diu"
    },
    {
      key: "DL",
      name: "Delhi"
    },
    {
      key: "GA",
      name: "Goa"
    },
    {
      key: "GJ",
      name: "Gujarat"
    },
    {
      key: "HR",
      name: "Haryana"
    },
    {
      key: "HP",
      name: "Himachal Pradesh"
    },
    {
      key: "JK",
      name: "Jammu and Kashmir"
    },
    {
      key: "JH",
      name: "Jharkhand"
    },
    {
      key: "KA",
      name: "Karnataka"
    },
    {
      key: "KL",
      name: "Kerala"
    },
    {
      key: "LD",
      name: "Lakshadweep"
    },
    {
      key: "MP",
      name: "Madhya Pradesh"
    },
    {
      key: "MH",
      name: "Maharashtra"
    },
    {
      key: "MN",
      name: "Manipur"
    },
    {
      key: "ML",
      name: "Meghalaya"
    },
    {
      key: "MZ",
      name: "Mizoram"
    },
    {
      key: "NL",
      name: "Nagaland"
    },
    {
      key: "OR",
      name: "Odisha"
    },
    {
      key: "PY",
      name: "Puducherry"
    },
    {
      key: "PB",
      name: "Punjab"
    },
    {
      key: "RJ",
      name: "Rajasthan"
    },
    {
      key: "SK",
      name: "Sikkim"
    },
    {
      key: "TN",
      name: "Tamil Nadu"
    },
    {
      key: "TS",
      name: "Telangana"
    },
    {
      key: "TR",
      name: "Tripura"
    },
    {
      key: "UK",
      name: "Uttar Pradesh"
    },
    {
      key: "UP",
      name: "Uttarakhand"
    },
    {
      key: "WB",
      name: "West Bengal"
    }
  ];


  voterDisposition = [
    { id: 1, name: "Select" },
    { id: 1, name: "Extremely Positive" },
    { id: 1, name: "Positive" },
    { id: 1, name: "Neutral" },
    { id: 1, name: "Negative" },
    { id: 1, name: "Extremely Negative" },

  ];
  voterStatus = [
    { id: 1, name: "Select" },
    { id: 1, name: "Not in position to vote" },
    { id: 1, name: "Voter shifted to other location" },
    { id: 1, name: "Voter shifted to other booth" },
    { id: 1, name: "No more voters" },

  ];

  religiousList=[
    { id: 1, name: "Select" },
    { id: 1, name: "Hindu General" },
    { id: 1, name: "Hindu SC" },
    { id: 1, name: "Hindu OBC" },
    { id: 1, name: "Hindu ST" },
    { id: 1, name: "Jain" },
    { id: 1, name: "Muslim" },
    { id: 1, name: "Christian" },
    { id: 1, name: "Sikh" },
  ]
  voteForMLA=[
    { id: 1, name: "Select" },
    { id: 1, name: "INC (Indian National Congress/ Congress )" },
    { id: 1, name: "BJP (Bharatiya Janata Party)" },
    { id: 1, name: "Regional Party 1" },
    { id: 1, name: "Regional Party 2" },
    { id: 1, name: "Independent" },
    { id: 1, name: "Undecided/ Yet to decide" },
    { id: 1, name: "Others (Please specify)" },
  ]

  transportation=[
    { id: 1, name: "Select" },
    { id: 1, name: "2 wheeler" },
    { id: 1, name: "4 wheeler" },
    { id: 1, name: "Tractor trolly" },
    { id: 1, name: "Bullock cart" },
    { id: 1, name: "Others (Please specify)" },
  ]
time=[
  { id: 1, name: "8:00 AM" },
  { id: 1, name: "9:00 AM" },
  { id: 1, name: "10:00 AM" },
  { id: 1, name: "11:00 AM" },
  { id: 1, name: "12:00 AM" },
  { id: 1, name: "1:00 PM" },
  { id: 1, name: "2:00 PM" },
  { id: 1, name: "3:00 PM" },
  { id: 1, name: "4:00 PM" },
  { id: 1, name: "5:00 PM" },
  { id: 1, name: "6:00 PM" },
  { id: 1, name: "8:00 PM" },
  { id: 1, name: "9:00 PM" },

]
constituencies=[
  {id: 1, name: "Narela"},
  {id: 2, name: "Burari"},
  {id: 3, name: "Timarpur"},
  {id: 4, name: "Adarsh Nagar"},
  {id: 5, name: "Badli"},
  {id: 6, name: "Rithala"},
  {id: 7, name: "Bawana"},
  {id: 8, name: "Mundka"},
  {id: 9, name: "Kirari"},
  {id: 10, name: "Sultan Pur Majra"},
  {id: 11, name: "Nangloi Jat"},
  {id: 12, name: "Mangol Puri"},
  {id: 13, name: "Rohini"},
  {id: 14, name: "Shalimar Bagh"},
  {id: 15, name: "Shakur Basti"},
  {id: 16, name: "Tri Nagar"},
  {id: 17, name: "Wazirpur"},
  {id: 18, name: "Model Town"},
  {id: 19, name: "Sadar Bazar"},
  {id: 20, name: "Chandni Chowk"},
  {id: 21, name: "Matia Mahal"},
  {id: 22, name: "Ballimaran"},
  {id: 23, name: "Karol Bagh"},
  {id: 24, name: "Patel Nagar"},
  {id: 25, name: "Moti Nagar"},
  {id: 26, name: "Madipur"},
  {id: 27, name: "Rajouri Garden"},
  {id: 28, name: "Hari Nagar"},
  {id: 29, name: "Tilak Nagar"},
  {id: 30, name: "Janakpuri"},
  {id: 31, name: "Vikaspuri"},
  {id: 32, name: "Uttam Nagar"},
  {id: 33, name: "Dwarka"},
  {id: 34, name: "Matiala"},
  {id: 35, name: "Najafgarh"},
  {id: 36, name: "Bijwasan"},
  {id: 37, name: "Palam"},
  {id: 38, name: "Delhi Cantonment"},
  {id: 39, name: "Rajinder Nagar"},
  {id: 40, name: "New Delhi"},
  {id: 41, name: "Jangpura"},
  {id: 42, name: "Kasturba Nagar"},
  {id: 43, name: "Malviya Nagar"},
  {id: 44, name: "R K Puram"},
  {id: 45, name: "Mehrauli"},
  {id: 46, name: "Chhatarpur"},
  {id: 47, name: "Deoli"},
  {id: 48, name: "Ambedkar Nagar"},
  {id: 49, name: "Sangam Vihar"},
  {id: 50, name: "Greater Kailash"},
  {id: 51, name: "Kalkaji"},
  {id: 52, name: "Tughlkabad extension"},
  {id: 53, name: "Badarpur"},
  {id: 54, name: "Okhla"},
  {id: 55, name: "Trilokpuri"},
  {id: 56, name: "Kondli"},
  {id: 57, name: "Patparganj"},
  {id: 58, name: "Laxmi Nagar"},
  {id: 59, name: "Vishwas Nagar"},
  {id: 60, name: "Krishna Nagar"},
  {id: 61, name: "Gandhi Nagar"},
  {id: 62, name: "Shahdara"},
  {id: 63, name: "Seemapuri"},
  {id: 64, name: "Rohtas Nagar"},
  {id: 65, name: "Seelampur"},
  {id: 66, name: "Ghonda"},
  {id: 67, name: "Babarpur"},
  {id: 68, name: "Gokalpur"},
  {id: 69, name: "Mustafabad"},
  {id: 70, name: "Karawal Nagar"}
];
districtlist=[
  { id: 1, name: "New Delhi" },
  { id: 2, name: "North Delhi" },
  { id: 3, name: "North West Delhi" },
  { id: 4, name: "West Delhi" },
  { id: 5, name: "South West Delhi" },
  { id: 6, name: "South Delhi" },
  { id: 7, name: "South East Delhi" },
  { id: 8, name: "Central Delhi" },
  { id: 9, name: "North East Delhi" },
  { id: 10, name: "Shahdara" },
  { id: 11, name: "	East Delhi" },

]

  ngOnInit() {
    // this.firstFormGroup = this._formBuilder.group({
    //   firstCtrl: ['', Validators.required]
    // });
    // this.secondFormGroup = this._formBuilder.group({
    //   secondCtrl: ['', Validators.required]
    // });
  }
  issueSelect() {
    console.log("topping", this.toppings);
  }
  deSelect(index) {
    this.toppings.value.splice(index, 1);
    console.log("topping", this.toppings.value);
  }
}

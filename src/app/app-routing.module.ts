import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LoginComponent } from './login/login/login.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { VoterDispositionComponent } from './content/voter-disposition/voter-disposition.component';
import { SocialMediaMgtComponent } from './content/social-media-mgt/social-media-mgt.component';
import { TopIssuesComponent } from './content/top-issues/top-issues.component';
import { VoterProfileComponent } from './content/voter-profile/voter-profile.component';
import { VotingDayComponent } from './content/voting-day/voting-day.component';
import { WorkersActivityComponent } from './content/workers-activity/workers-activity.component';
import { ForgotPasswordComponent } from './login/forgot-password/forgot-password.component';
import { ChangePasswordComponent } from './login/change-password/change-password.component';
import { ClientUserComponent } from './user-creation/client-user/client-user.component';
import { AccountSettingsComponent } from './user-creation/account-settings/account-settings.component';
import { MapComponent } from './content/map/map.component';
import { PieChartComponent } from './content/chart/pie-chart/pie-chart.component';
import { DoughnutChartComponent } from './content/chart/doughnut-chart/doughnut-chart.component';


const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full'
  },
  // login Section routing

  { path: 'login', component: LoginComponent },
  { path: 'reset-passward', component: ForgotPasswordComponent },
  { path: 'change-passward', component: ChangePasswordComponent },
  //  dashboard Section routing

  {
    path: 'home', component: DashboardComponent, children: [
      { path: 'dashboard', component: VoterDispositionComponent },
      { path: 'voter-data', component: VotingDayComponent },
      { path: 'social-media-mgt', component: SocialMediaMgtComponent },
      { path: 'map', component: MapComponent },

      { path: 'pai-chart', component: DoughnutChartComponent },


      { path: 'top-issues', component: TopIssuesComponent },
      { path: 'voter-profile', component: VoterProfileComponent },
      { path: 'workers-activity', component: WorkersActivityComponent },
      { path: 'user-cration', component: ClientUserComponent },
      { path: 'account-settings', component: AccountSettingsComponent },

    ]
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }

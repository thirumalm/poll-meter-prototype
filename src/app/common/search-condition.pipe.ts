import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'searchCondition'
})
export class SearchConditionPipe implements PipeTransform {

  transform(dataList: any, searchKey: String, type: String): any {
    if (!dataList) return [];
    if (!searchKey) return dataList;
    searchKey = searchKey.toLowerCase();
    if (searchKey.length >= 2) {
      return dataList.filter(
        datas => {
          if (type == 'LokSabhaNo') {
            return datas.value.toString().toLowerCase().includes(searchKey);
          } else if (type == 'lokSabha') {
            return datas.name.toString().toLowerCase().includes(searchKey);
          }
          else if (type == 'VidhanSabha') {
            return datas.name.toString().toLowerCase().includes(searchKey);
          }
          else if (type == 'VidhanSabhaNo') {
            return datas.value.toString().toLowerCase().includes(searchKey);
          }
          else if (type == 'PollingBooth') {
            return datas.value.toString().toLowerCase().includes(searchKey);
          }
          else if (type == 'Village') {
            return datas.value.toString().toLowerCase().includes(searchKey);
          }
          else if (type == 'WardNo') {
            return datas.value.toString().toLowerCase().includes(searchKey);
          }
          else if (type == 'Locality') {
            return datas.value.toString().toLowerCase().includes(searchKey);
          }
          else if (type == 'Name') {
            return datas.value.toString().toLowerCase().includes(searchKey);
          }
          else if (type == 'Address') {
            return datas.value.toString().toLowerCase().includes(searchKey);
          }
        });
    } else {
      return dataList;
    }
  }

}

import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-account-settings',
  templateUrl: './account-settings.component.html',
  styleUrls: ['./account-settings.component.scss']
})
export class AccountSettingsComponent implements OnInit {
  userName='Test Client'
  mobileNo='7602066932'
  isEditable=true;
  constructor() { }

  ngOnInit() {
  }

}

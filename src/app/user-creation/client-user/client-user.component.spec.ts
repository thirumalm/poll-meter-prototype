import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ClintUserComponent } from './client-user.component';

describe('ClintUserComponent', () => {
  let component: ClintUserComponent;
  let fixture: ComponentFixture<ClintUserComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ClintUserComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ClintUserComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VoterDataComponent } from './voter-data.component';

describe('VoterDataComponent', () => {
  let component: VoterDataComponent;
  let fixture: ComponentFixture<VoterDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VoterDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VoterDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
